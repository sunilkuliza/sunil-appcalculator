package com.example.kuliza123.appcalculator.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kuliza123.appcalculator.Models.Category;
import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;

import java.util.ArrayList;

/**
 * Created by kuliza214 on 3/8/15.
 */
public class OverViewListAdapter extends RecyclerView.Adapter<OverViewListAdapter.ListHolder>{
    private ArrayList<Category> mOverviewList;
    private Context mContext;
    private int mColomns,mEmptyFront,mEmptyEnd;
    private int mListIndex,mItemIndex;
    private ArrayList<PlatformListModel> mFinaList;
    public OverViewListAdapter(Context context,ArrayList<Category> overviewList,int colomns) {
        mFinaList = new ArrayList<PlatformListModel>();
        mContext = context;
        mOverviewList = overviewList;
        createFinalList();
        mColomns = colomns;
        mListIndex =0;
        mItemIndex =0;
    }

    private void createFinalList() {
        for(int i=0;i<mOverviewList.size();i++){
            for(int j=0;j<mOverviewList.get(i).mList.size();j++){
                String string = mOverviewList.get(i).mList.get(j).getName();
                //mOverviewList.get(i).mList.get(j).setName(Constants.KEY[i]+":"+string);
                mOverviewList.get(i).mList.get(j).setName(string);
                mFinaList.add(mOverviewList.get(i).mList.get(j));
            }
        }
    }

    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gridview_items,parent,false);
        ((TextView)view.findViewById(R.id.categoryName)).setTextSize(12);
        //View convertView  = layoutInflater.inflate(R.layout.gridview_items,parent,false);
        ListHolder listHolder = new ListHolder(view);
        return listHolder;
    }

    @Override
    public void onBindViewHolder(ListHolder holder, int position) {
        if(holder == null){

        }
        holder.mListImage.setImageResource(mFinaList.get(position).getImage());
        holder.mListName.setText( mFinaList.get(position).getName());

        //holder.mListName.setText(mFinaList.get(position).getName());
    }
    /*public int getMaxLenght(){
        int max = 0;
        for(int i=0;i<mOverviewList.size();i++){
            if(mOverviewList.get(i).mList.size()>max){
                max = mOverviewList.get(i).mList.size();
            }
        }
        return max;
    }*/
    @Override
    public int getItemCount() {
        return mFinaList.size();
    }

    public int getSize() {
        int size =0;
        for(int i=0;i<mOverviewList.size();i++){
            size = size + mOverviewList.get(i).mList.size();
        }
        return size;
    }

    public class ListHolder extends RecyclerView.ViewHolder{
        private ImageView mListImage ;
        private TextView mListName;

        public ListHolder(View itemView) {
            super(itemView);
            mListImage = (ImageView) itemView.findViewById(R.id.categoryImage);
            mListName = (TextView) itemView.findViewById(R.id.categoryName);

        }
    }
}