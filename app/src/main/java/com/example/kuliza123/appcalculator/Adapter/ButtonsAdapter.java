package com.example.kuliza123.appcalculator.Adapter;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.kuliza123.appcalculator.Activities.MainActivity;
import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;

import java.security.Policy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.Inflater;

/**
 * Created by kuliza214 on 13/8/15.
 */
public class ButtonsAdapter extends RecyclerView.Adapter<ButtonsAdapter.ItemHolder> {
    private int mButonsount;
    private Context mContext;
    private int mFragmentCounter;
    public CallBackToActivity mListner;
    private HashMap<String,ArrayList<PlatformListModel>> mHashMapOfOptions;
    public ButtonsAdapter(Context context,int count,int fragmentCounter,HashMap<String, ArrayList<PlatformListModel>> optionsAppCalculatorHashMap){
        mButonsount = count;
        mContext = context;
        mFragmentCounter = fragmentCounter;
        mHashMapOfOptions = optionsAppCalculatorHashMap;
    }

    @Override
    public ButtonsAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.button_item,parent,false);
        ButtonsAdapter.ItemHolder itemHolder = new ItemHolder(view);
        mListner = (CallBackToActivity) mContext;
        return itemHolder;
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, final int position) {
        if(position > Constants.ZERO_POINTER) {
            holder.btnButton.setText(String.valueOf(position + 1));
            //holder.btnButton.findViewById(R.id.btnIndicator);
        }
        else{
            int num = position+1;
            holder.btnButton.setText("0"+num);
        }
        if(position < mFragmentCounter){
            holder.btnButton.setBackgroundColor(mContext.getResources().getColor(R.color.compleated_tile));
            holder.btnButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*if () {

                    }*/
                }
            });
            holder.imgIndicator.setVisibility(View.GONE);
        }
        if(position == mFragmentCounter){
            holder.btnButton.setBackgroundResource(R.drawable.green_button);
            //holder.btnButton.setTextSize;
            holder.btnButton.setWidth(R.dimen.selected_image_width);
            holder.btnButton.setHeight(R.dimen.selected_image_height);
            holder.imgIndicator.setVisibility(View.VISIBLE);
        }
        else if(position > mFragmentCounter){
            holder.btnButton.setBackgroundColor(mContext.getResources().getColor(R.color.un_selected_tile));
            if(mFragmentCounter + 1 == position){
                holder.btnButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Checking weather atleast one item is selected or not
                        ArrayList<PlatformListModel> list = mHashMapOfOptions.get(Constants.KEY[mFragmentCounter]);
                        if(atleastOneOfThemIsSelected(list)) {
                            mListner.onNextClick();
                        }
                    }
                });
            }
            holder.imgIndicator.setVisibility(View.GONE);
        }
    }

    private boolean atleastOneOfThemIsSelected(ArrayList<PlatformListModel> list) {
        for(int i=0;i<list.size();i++){
            if(list.get(i).getIsSelected()){
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return mButonsount;
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        public Button btnButton;
        public ImageView imgIndicator;
        public ItemHolder(View itemView) {
            super(itemView);
            btnButton = (Button) itemView.findViewById(R.id.btnButton);
            imgIndicator = (ImageView) itemView.findViewById(R.id.imgIndicator);
        }
    }
    public interface CallBackToActivity{
        public void onPreviousButtonClick(int position);
        public void onNextClick();
    }
}