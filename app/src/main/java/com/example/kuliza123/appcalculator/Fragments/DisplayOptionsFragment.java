package com.example.kuliza123.appcalculator.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kuliza123.appcalculator.Adapter.GridViewAdapter;
import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DisplayOptionsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DisplayOptionsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DisplayOptionsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View mView;
    private RecyclerView mRecyclerGridView;
    private GridViewAdapter mGridViewAdapter;
    private GridLayoutManager mGridLayoutManager;
    private FloatingActionButton mNextBtn;
    private CoordinatorLayout mCoordinatorLayout;
    private ArrayList<PlatformListModel> optionsArrayList;
    private int FRAGMENT_COUNTER;
    private FragmentManager fragmentManager;
    private TextView mTvHeader;
    private TextView mTvFooter;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DisplayOptionsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DisplayOptionsFragment newInstance(String param1, String param2) {
        return new DisplayOptionsFragment();
    }

    public DisplayOptionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            optionsArrayList = getArguments().getParcelableArrayList("ArrayListData");
            FRAGMENT_COUNTER = getArguments().getInt(Constants.FRAGMENT_COUNTER);
        }
        fragmentManager = getFragmentManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.platform_fragment_screen, container, false);
        toolbarSetUp();
        //Header and footer Textview
        mTvHeader = (TextView) mView.findViewById(R.id.tvHeaderMesssage);
        mTvFooter = (TextView) mView.findViewById(R.id.tvFooterMessage);
        mTvHeader.setText(Constants.HEADER_STRINGS[FRAGMENT_COUNTER]);
        if(Constants.isSingleSelect[FRAGMENT_COUNTER]){
            mTvFooter.setText(Constants.SINGLE_OPTION);
        }
        else{
            mTvFooter.setText(Constants.MULTIPLE_OPTION);
        }
        mCoordinatorLayout = (CoordinatorLayout) mView.findViewById(R.id.main_content);
        mRecyclerGridView = (RecyclerView) mView.findViewById(R.id.recyclerView);
        mGridLayoutManager = new GridLayoutManager(getActivity(),Constants.GRIDVIEW_COLOMNS);
        mRecyclerGridView.setLayoutManager(mGridLayoutManager);
        //mPlatformList should be updated according to Hashmap
        mGridViewAdapter = new GridViewAdapter(getActivity(), optionsArrayList,Constants.isSingleSelect[FRAGMENT_COUNTER],priceCallback);
        mRecyclerGridView.setAdapter(mGridViewAdapter);
        //Snackbar.make(mCoordinatorLayout, Constants.OPTIONS_MESSAGES[FRAGMENT_COUNTER], Snackbar.LENGTH_LONG).show();
        mNextBtn=(FloatingActionButton) mView.findViewById(R.id.fab);
       /* if(FRAGMENT_COUNTER == 14){
            mNextBtn.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    mListener.onFragmentInteraction();
                }
            });
            return mView;
        }*/
        mNextBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(atLeatOneOfthemIsSelected() == false){
                    Snackbar.make(mView,"You are required to select atleat one option",Snackbar.LENGTH_LONG).show();
                }
                if(atLeatOneOfthemIsSelected()) {
                    mListener.onFragmentInteraction();
                }
            }
        });
        return mView;
    }

    private boolean atLeatOneOfthemIsSelected() {
        for(int i=0;i<optionsArrayList.size();i++){
            if(optionsArrayList.get(i).getIsSelected()){
                return true;
            }
        }
        return false;
    }

    private void toolbarSetUp() {
        //Log.i("Fragment Check",String.valueOf(FRAGMENT_COUNTER)+" "+String.valueOf(fragmentManager.getBackStackEntryCount()));
        Toolbar toolbar =(Toolbar) getActivity().findViewById(R.id.toolbar);
        if (FRAGMENT_COUNTER<=0){
            toolbar.findViewById(R.id.back_button).setVisibility(View.GONE);
        }else {
            toolbar.findViewById(R.id.back_button).setVisibility(View.VISIBLE);
        }
        TextView textView = (TextView)toolbar.findViewById(R.id.toolbar_text);
        textView.setText(Constants.KEY[FRAGMENT_COUNTER]);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction();
        public void updateprice();
    }
    GridViewAdapter.UpdatePriceCallback priceCallback =new GridViewAdapter.UpdatePriceCallback() {
        @Override
        public void calculatePrice() {
            mListener.updateprice();
        }
    };
}
