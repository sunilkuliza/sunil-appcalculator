package com.example.kuliza123.appcalculator.Fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kuliza123.appcalculator.Adapter.OverViewListAdapter;
import com.example.kuliza123.appcalculator.Models.Category;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OverviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OverviewFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private int mTime;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OverviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OverviewFragment newInstance(String param1, String param2) {
        OverviewFragment fragment = new OverviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private ArrayList<Category> mCategoryList;
    public OverviewFragment(){

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mCategoryList = getArguments().getParcelableArrayList(Constants.OVERVIEW_KEY);
        mTime = getArguments().getInt(Constants.TIME_KEY);
        Log.d("Size",":"+mCategoryList.size());
        View rootView = inflater.inflate(R.layout.fragment_overview, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        TextView timeView  = (TextView)rootView.findViewById(R.id.time);
        TextView amountview  = (TextView)rootView.findViewById(R.id.amount);
        timeView.setText("Estimated Time : "+ mTime +" hrs");
        amountview.setText("Estimated Price : $ "+mTime*25);
        //recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),getMaxLenght()));
       // OverViewListAdapter overViewListAdapter = new OverViewListAdapter(getActivity(),mCategoryList,getMaxLenght());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        OverViewListAdapter overViewListAdapter = new OverViewListAdapter(getActivity(),mCategoryList,2);
        recyclerView.setAdapter(overViewListAdapter);
        return rootView;
    }

    private void setupToolBar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView toolBarTitle = (TextView) toolbar.findViewById(R.id.toolbar_text);
        toolBarTitle.setText(Constants.ALL_SELECTION);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    public int getMaxLenght(){
        int max = 0;
        for(int i=0;i<mCategoryList.size();i++){
            if(mCategoryList.get(i).mList.size()>max){
                max = mCategoryList.get(i).mList.size();
            }
        }
        return max;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupToolBar();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
           // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}
