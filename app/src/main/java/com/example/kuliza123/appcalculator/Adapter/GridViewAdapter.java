package com.example.kuliza123.appcalculator.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;

import java.util.List;


/**
 * Created by kuliza123 on 29/7/15.
 */
public class GridViewAdapter extends RecyclerView.Adapter<GridViewAdapter.ListHolder> {

    private Context mContext;
    private List<PlatformListModel> mList;
    Boolean mSingleSelect;
    UpdatePriceCallback mUpdatePrice;

    public GridViewAdapter(Context context,List<PlatformListModel>  list, Boolean singleSelect, UpdatePriceCallback priceCallback ){
        mContext=context;
        mList=list;
        mSingleSelect=singleSelect;
        mUpdatePrice=priceCallback;
    }

    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.gridview_items, parent, false);*/
        View view = LayoutInflater.from(mContext).inflate(R.layout.gridview_items,parent,false);
        ListHolder mListHolder = new ListHolder(view);
        return mListHolder;
    }

    @Override
    public void onBindViewHolder(final ListHolder mListHolder, final int position) {
        mListHolder.mListName.setText(mList.get(position).getName().toString());
        if(mList.get(position).getIsSelected()){
           // mListHolder.mListImage.setImageResource(R.drawable.selected);
            //mListHolder.mListImage.setBackground(Drawable.createFromPath(String.valueOf(mList.get(position).getImage())));
            mListHolder.mListImage.setImageResource(mList.get(position).getImage());
            mListHolder.mImageSelected.setVisibility(View.VISIBLE);
        }
        else
        {
            mListHolder.mListImage.setImageResource(mList.get(position).getImage());
            mListHolder.mImageSelected.setVisibility(View.GONE);
        }
        if(mSingleSelect){
            mListHolder.mListImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    if(mList.get(position).getIsSelected()){
                        unSelectOthers();
                        mList.get(position).setIsSelected(false);
                        notifyDataSetChanged();
                        mUpdatePrice.calculatePrice();
                    }
                    else{
                        unSelectOthers();
                        mList.get(position).setIsSelected(true);
                        notifyDataSetChanged();
                        mUpdatePrice.calculatePrice();
                    }
                }
            });
        }
        if(mSingleSelect == false) {
            mListHolder.mListImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mList.get(position).getIsSelected()) {
                        if (mList.get(position).getName().equals("N/A") || mList.get(position).getName().equals("No") || mList.get(position).getName().equals("None")
                                || mList.get(position).getName().equals("Not Important") || mList.get(position).getName().equals("No Major Back End Logic") ||
                                mList.get(position).getName().equals("Not Required")) {
                            unSelectOthers();
                            notifyDataSetChanged();
                        }
                        else {
                            unSelectNoOption();
                            notifyDataSetChanged();

                        }
                        //mListHolder.mListImage.setImageResource(R.drawable.selected);

                        mListHolder.mListImage.setImageResource(mList.get(position).getImage());
                        mListHolder.mImageSelected.setVisibility(View.VISIBLE);
                        mList.get(position).setIsSelected(true);
                        //notifyDataSetChanged();
                        mUpdatePrice.calculatePrice();
                    }
                    else {
                        mListHolder.mListImage.setImageResource(mList.get(position).getImage());
                        mList.get(position).setIsSelected(false);
                        mListHolder.mImageSelected.setVisibility(View.GONE);
                        mUpdatePrice.calculatePrice();
                    }
                }
                public void unSelectNoOption(){
                    if (mList.get(0).getName().equals("N/A") || mList.get(0).getName().equals("No") || mList.get(0).getName().equals("None")
                            || mList.get(0).getName().equals("Not Important") || mList.get(0).getName().equals("No Major Back End Logic") ||
                            mList.get(0).getName().equals("Not Required")) {
                        mList.get(0).setIsSelected(false);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ListHolder extends RecyclerView.ViewHolder {
        private ImageView mListImage;
        private TextView mListName;
        private ImageView mImageSelected;

        public ListHolder(View itemView) {
            super(itemView);
            mListImage = (ImageView) itemView.findViewById(R.id.categoryImage);
            mListName = (TextView) itemView.findViewById(R.id.categoryName);
            mImageSelected = (ImageView) itemView.findViewById(R.id.imageSelected);
        }
    }

    public interface UpdatePriceCallback{
        public void calculatePrice();
        //public void unSelectAll
    }

    public void unSelectOthers(){
        for(int i=0;i<mList.size();i++) {
            mList.get(i).setIsSelected(false);
        }
    }
    }