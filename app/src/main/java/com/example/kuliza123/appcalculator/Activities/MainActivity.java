package com.example.kuliza123.appcalculator.Activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.kuliza123.appcalculator.Adapter.ButtonsAdapter;
import com.example.kuliza123.appcalculator.Fragments.DisplayOptionsFragment;
import com.example.kuliza123.appcalculator.Fragments.OverviewFragment;
import com.example.kuliza123.appcalculator.Models.Category;
import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements
        DisplayOptionsFragment.OnFragmentInteractionListener,
        ButtonsAdapter.CallBackToActivity{
    public int FRAGMENT_COUNTER = 0;
    private DisplayOptionsFragment mDisplayOptionsFragment;
    private RelativeLayout backButtonLayout;
    private TextView mCost;

    public HashMap<String, ArrayList<PlatformListModel>> optionsAppCalculatorHashMap = new HashMap<>();
    private Toolbar toolbar;
    private FragmentTransaction fragmentTransaction;
    private int mTotalPrice, mMultiplier;
    private ArrayList<Category> overviewList;
    private int time;
    private int mTotalTime;
    private TextView mTvHeader,mTvFooter;
    private ArrayList<PlatformListModel> createPlatformList() {
        ArrayList<PlatformListModel> platformList = new ArrayList<>();
        platformList.add(new PlatformListModel(R.drawable.iosphone, 0, "IOS", false, 0));
        platformList.add(new PlatformListModel(R.drawable.androidphone, 0, "Android", false, 0));
        platformList.add(new PlatformListModel(R.drawable.windows_phone, 0, "Windows", false, 0));
        platformList.add(new PlatformListModel(R.drawable.blackberry, 0, "BlackBerry", false, 0));
        platformList.add(new PlatformListModel(R.drawable.web, 0, "Web", false, 0));
        return platformList;
    }

    private ArrayList<PlatformListModel> createScreenCountList() {
        ArrayList<PlatformListModel> screenList = new ArrayList<>();
        screenList.add(new PlatformListModel(R.drawable.small, 600, "Small(1-3 Screens)", false, 24));
        screenList.add(new PlatformListModel(R.drawable.medium, 1500, "Medium(3-8 Screens)", false, 60));
        screenList.add(new PlatformListModel(R.drawable.large, 3600, "Large(9+ Screens)", false, 144));
        return screenList;
    }

    private ArrayList<PlatformListModel> createUserMagementList() {
        ArrayList<PlatformListModel> userMagementList = new ArrayList<>();
        userMagementList.add(new PlatformListModel(R.drawable.none, 0, "N/A", false, 0));
        userMagementList.add(new PlatformListModel(R.drawable.standard, 500, "Standard", false, 20));
        userMagementList.add(new PlatformListModel(R.drawable.social, 400, "Social", false,  16));
        userMagementList.add(new PlatformListModel(R.drawable.enterprise, 750, "Enterprise", false, 30));
        return userMagementList;
    }
    private RecyclerView mButtonsView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        generateHashMap();
        mButtonsView = (RecyclerView) findViewById(R.id.buttonsFrame);
        mButtonsView.setLayoutManager(new GridLayoutManager(this, Constants.SLIDER_COLOMNS));
        mButtonsView.setAdapter(new ButtonsAdapter(this, Constants.KEY.length, FRAGMENT_COUNTER,optionsAppCalculatorHashMap));

        mCost = (TextView) findViewById(R.id.cost);
        setupToolbar(false);
        TextView textView = (TextView) toolbar.findViewById(R.id.toolbar_text);
        textView.setText("Platform's");
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("ArrayListData", optionsAppCalculatorHashMap.get(Constants.KEY[FRAGMENT_COUNTER]));
        bundle.putInt(Constants.FRAGMENT_COUNTER, FRAGMENT_COUNTER);
        mDisplayOptionsFragment = DisplayOptionsFragment.newInstance(null, null);
        mDisplayOptionsFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, mDisplayOptionsFragment, Constants.KEY[FRAGMENT_COUNTER]).addToBackStack(null).commit();
    }

    private void generateHashMap() {
        optionsAppCalculatorHashMap.put(Constants.KEY[0], createPlatformList());
        optionsAppCalculatorHashMap.put(Constants.KEY[1], createScreenCountList());
        optionsAppCalculatorHashMap.put(Constants.KEY[2], createUserMagementList());
        optionsAppCalculatorHashMap.put(Constants.KEY[3], createDataList());
        optionsAppCalculatorHashMap.put(Constants.KEY[4], createDataSourcesList());
        optionsAppCalculatorHashMap.put(Constants.KEY[5], createLocationList());
        optionsAppCalculatorHashMap.put(Constants.KEY[6], createThirdPartyCloudList());
        optionsAppCalculatorHashMap.put(Constants.KEY[7], createEngageUsersList());
        optionsAppCalculatorHashMap.put(Constants.KEY[8], createQualityList());
        optionsAppCalculatorHashMap.put(Constants.KEY[9], createSecurityList());
        optionsAppCalculatorHashMap.put(Constants.KEY[10], createAnalyticsList());
        optionsAppCalculatorHashMap.put(Constants.KEY[11], createReliabilityList());
        optionsAppCalculatorHashMap.put(Constants.KEY[12], createWebPortalList());
        optionsAppCalculatorHashMap.put(Constants.KEY[13], createBackEndList());
        optionsAppCalculatorHashMap.put(Constants.KEY[14], createOfflineList());
    }

    private ArrayList<PlatformListModel> createOfflineList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "Not Required", false, 0));
        dataList.add(new PlatformListModel(R.drawable.required, 500, "Required", false, 20));
        return dataList;
    }

    private ArrayList<PlatformListModel> createBackEndList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "No Major Back End Logic", false, 0));
        dataList.add(new PlatformListModel(R.drawable.builtwithservice, 500, "Backend Build With Ready Services", false, 20));
        dataList.add(new PlatformListModel(R.drawable.builtwithoutservice, 1000, "Backend Build not Service Enabled", false, 40));
        dataList.add(new PlatformListModel(R.drawable.to_create, 2000, "Backend Needs To Be Created", false, 80));
        return dataList;
    }

    private ArrayList<PlatformListModel> createWebPortalList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "Not Important", false, 0));
        dataList.add(new PlatformListModel(R.drawable.important_web, 1000, "Important", false, 40));
        return dataList;
    }

    private ArrayList<PlatformListModel> createReliabilityList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "Not Important", false, 0));
        dataList.add(new PlatformListModel(R.drawable.important, 1250, "Important", false, 50));
        return dataList;
    }

    private ArrayList<PlatformListModel> createAnalyticsList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "None", false, 0));
        dataList.add(new PlatformListModel(R.drawable.minimal, 350, "Minimal", false, 14));
        dataList.add(new PlatformListModel(R.drawable.everything, 750, "Everything", false, 30));
        return dataList;
    }

    private ArrayList<PlatformListModel> createSecurityList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "N/A", false, 0));
        dataList.add(new PlatformListModel(R.drawable.complete, 800, "Complete", false, 32));
        return dataList;
    }

    private ArrayList<PlatformListModel> createQualityList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.mvp, 500, "MVP", false, 20));
        dataList.add(new PlatformListModel(R.drawable.polished, 1250, "Polished", false, 50));
        return dataList;
    }

    private ArrayList<PlatformListModel> createEngageUsersList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "N/A", false, 0));
        dataList.add(new PlatformListModel(R.drawable.push, 600, "Push, SMS, Email", false, 24));
        dataList.add(new PlatformListModel(R.drawable.advancesocialintegration, 600, "Advanced social \n" +"integration", false, 24));
        return dataList;
    }

    private ArrayList<PlatformListModel> createThirdPartyCloudList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "No", false,0));
        dataList.add(new PlatformListModel(R.drawable.simple_third_party_cloud, 300, "Simple", false, 12));
        dataList.add(new PlatformListModel(R.drawable.advancedthirdpartycloud, 500, "Advanced", false, 20));
        return dataList;
    }

    private ArrayList<PlatformListModel> createLocationList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "No", false, 0));
        dataList.add(new PlatformListModel(R.drawable.simple_map, 400, "Simple", false, 16));
        dataList.add(new PlatformListModel(R.drawable.advancedmap, 800, "Advanced", false, 32));
        return dataList;
    }

    private ArrayList<PlatformListModel> createDataSourcesList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "No", false, 0));
        dataList.add(new PlatformListModel(R.drawable.simple, 400, "Simple", false, 16));
        dataList.add(new PlatformListModel(R.drawable.advanced, 700, "Advanced", false, 28));
        return dataList;
    }

    private ArrayList<PlatformListModel> createDataList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, 0, "N/A", false, 0));
        dataList.add(new PlatformListModel(R.drawable.data, 350, "Data", false, 14));
        dataList.add(new PlatformListModel(R.drawable.largefiles, 750, "Large Files", false, 30));
        return dataList;
    }

    public void setupToolbar(Boolean backVisible) {
        // Set Toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        backButtonLayout = (RelativeLayout) toolbar.findViewById(R.id.back_button);
        if (backVisible) {
            toolbar.findViewById(R.id.back_button).setVisibility(View.VISIBLE);
        } else if (backVisible == false) {
            toolbar.findViewById(R.id.back_button).setVisibility(View.GONE);
        }
        backButtonLayout.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        FRAGMENT_COUNTER--;
        FragmentManager fragmentManager = getFragmentManager();
        if(FRAGMENT_COUNTER == 14){
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
            linearLayout.setVisibility(View.VISIBLE);
        }
        if (fragmentManager.getBackStackEntryCount() > 1) {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            fragmentManager.popBackStack();
            mButtonsView.setAdapter(new ButtonsAdapter(this,Constants.KEY.length,FRAGMENT_COUNTER,optionsAppCalculatorHashMap));
        }
        else {
            finish();
        }
    }


    @Override
    public void onFragmentInteraction() {
        Log.d("Fragment Counter", FRAGMENT_COUNTER + "");
        if(FRAGMENT_COUNTER == 14){
            FRAGMENT_COUNTER++;
            createOverViewArryList();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constants.OVERVIEW_KEY, (ArrayList<? extends android.os.Parcelable>) overviewList);
            bundle.putInt(Constants.TIME_KEY,mTotalTime);
            OverviewFragment overviewFragment = new OverviewFragment();
            overviewFragment.setArguments(bundle);
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
            linearLayout.setVisibility(View.GONE);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
            fragmentTransaction.replace(R.id.content_frame,overviewFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            return;
        }
        FRAGMENT_COUNTER++;
        mButtonsView.setAdapter(new ButtonsAdapter(this,Constants.KEY.length,FRAGMENT_COUNTER,optionsAppCalculatorHashMap));
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("ArrayListData", optionsAppCalculatorHashMap.get(Constants.KEY[FRAGMENT_COUNTER]));
        bundle.putInt(Constants.FRAGMENT_COUNTER, FRAGMENT_COUNTER);
        mDisplayOptionsFragment = DisplayOptionsFragment.newInstance(null, null);
        mDisplayOptionsFragment.setArguments(bundle);
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.content_frame, mDisplayOptionsFragment, Constants.KEY[FRAGMENT_COUNTER]);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void createOverViewArryList() {
        overviewList = new ArrayList<Category>();
        for(int i=0;i<optionsAppCalculatorHashMap.size();i++){
            Category category = new Category();
            ArrayList<PlatformListModel> list = optionsAppCalculatorHashMap.get(Constants.KEY[i]);
            for(int j=0;j<list.size();j++){
                if(list.get(j).getIsSelected()){
                    category.addItem(list.get(j));
                }
            }
            overviewList.add(category);
        }
    }

    @Override
    public void updateprice() {
        updatedUpdatePrice();
    }

    public void updatedUpdatePrice(){
        mTotalPrice = 0;
        mMultiplier = 0;
        mTotalTime = 0;
        ArrayList<PlatformListModel> Temp1 = new ArrayList<>();
        Temp1=optionsAppCalculatorHashMap.get(Constants.KEY[0]);
        for (PlatformListModel x : Temp1) {
            if (x.getIsSelected()) {
                mMultiplier += 1;
            }
        }

        if(optionsAppCalculatorHashMap.get(Constants.KEY[1]).get(0).getIsSelected()){
            Log.e("Platform : ",optionsAppCalculatorHashMap.get(Constants.KEY[1]).get(0).getName());
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(0).setTime(0);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(1).setTime(20);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(2).setTime(40);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(3).setTime(80);
            optionsAppCalculatorHashMap.get(Constants.KEY[14]).get(1).setTime(20);
        }
        if(optionsAppCalculatorHashMap.get(Constants.KEY[1]).get(1).getIsSelected()){
            Log.e("Platform : ",optionsAppCalculatorHashMap.get(Constants.KEY[1]).get(0).getName());
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(0).setTime(0);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(1).setTime(40);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(2).setTime(80);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(3).setTime(160);
            optionsAppCalculatorHashMap.get(Constants.KEY[14]).get(1).setTime(40);
        }
        if(optionsAppCalculatorHashMap.get(Constants.KEY[1]).get(2).getIsSelected()){
            Log.e("Platform : ",optionsAppCalculatorHashMap.get(Constants.KEY[1]).get(0).getName());
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(0).setTime(0);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(1).setTime(60);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(2).setTime(120);
            optionsAppCalculatorHashMap.get(Constants.KEY[13]).get(3).setTime(240);
            optionsAppCalculatorHashMap.get(Constants.KEY[14]).get(1).setTime(60);
        }
        for(int i=1;i<Constants.KEY.length;i++){
            ArrayList<PlatformListModel> Temp2 = new ArrayList<>();
            Temp2=optionsAppCalculatorHashMap.get(Constants.KEY[i]);
            for (PlatformListModel x : Temp2) {
                if (x.getIsSelected()) {
                    mTotalTime = mTotalTime + mMultiplier*x.getTime();
                }
            }
        }
        if(optionsAppCalculatorHashMap.get(Constants.KEY[12]).get(1).getIsSelected() && atleatOnePlatformIsSelected(optionsAppCalculatorHashMap.get(Constants.KEY[0]))) {
            mTotalTime = mTotalTime - optionsAppCalculatorHashMap.get(Constants.KEY[12]).get(1).getTime() * mMultiplier + optionsAppCalculatorHashMap.get(Constants.KEY[12]).get(1).getTime();
        }
        mTotalPrice = mTotalTime*25;
        mCost.setText("$ "+mTotalTime*25);
    }

    private boolean atleatOnePlatformIsSelected(ArrayList<PlatformListModel> platformListModels) {
        for(int i=0;i<platformListModels.size();i++){
            if(platformListModels.get(i).getIsSelected()){
                return true;
            }
        }
        return false;
    }

    public int getTime() {
        int time = 0;
        for(int i=0;i<overviewList.size();i++){
            for(int j=0;j<overviewList.get(i).mList.size();j++){
                if(overviewList.get(i).mList.get(j).getIsSelected()) {
                    time = time + overviewList.get(i).mList.get(j).getTime();
                }
            }
        }
        return time;
    }

    @Override
    public void onPreviousButtonClick(int position) {

    }

    @Override
    public void onNextClick() {
        onFragmentInteraction();
    }
}