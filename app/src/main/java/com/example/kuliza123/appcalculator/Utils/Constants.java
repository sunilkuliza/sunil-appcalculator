package com.example.kuliza123.appcalculator.Utils;

/**
 * Created by kuliza123 on 29/7/15.
 */
public class Constants {
    public static final String FRAGMENT_COUNTER = "Fragment_Counter";
    public static final String ADD_PLATFORM_MESSAGE = "Add the desired Platforms";
    public static final String ADD_SCREEN_MESSAGE = "Add Expected Screens";
    public static final String ADD_ENTERPRISE_MESSAGE = "Add Enterprise Level";
    public static final String ADD_DATASOURCE_MESSAGE = "Add DataSource Level";
    public static final String ADD_DATASTORAGE_MESSAGE = "Add Data Storage Methods";
    public static final String ADD_Location_MESSAGE = "Add Location Options";
    public static final String ADD_CLOUD_MESSAGE = "Add Cloud Options";
    public static final String ADD_USER_MESSAGE = "Add User Engagement Level";
    public static final String ADD_QUALITY_MESSAGE = "Add Quality Experience Level";
    public static final String ADD_SECURITY_MESSAGE = "Add Security Level";
    public static final String ADD_ANALYTICS_MESSAGE = "Add Analytics Level";
    public static final String ADD_SCALIBILITY_MESSAGE = "Add Relability/Scalability Level";
    public static final String ADD_PORTAL_MESSAGE = "Whether Web Portal is needed";
    public static final String ADD_BACKEND_APPLICATIONS= "Will your app leverage an existing backend application?";
    public static final String ADD_OFFLINE_MESSAGE = "Will Offline Mode will be required";
    public static final String ALL_SELECTION = "All Selection";
    public static final String TIME_KEY ="time key" ;

    public static String platformFragment = "Platform_Fragment";
    public static final String  ScreenFragement = "Screenfragment";
    public static String platformheadText = "What platform will your apps run on?";
    public static String ScreenHeadText = "How big is your app going to be?";
    public static String UserManagementHeadText = "How will you handle user management?";
    public static String DataKindHeadText = "What kind of data will you store?";
    public static String DataSourcesheadText = "Will you need to integrate any existing data sources?";
    public static String LocationHeadText = "Will your app use location data?";
    public static String ThirdpartyHeadText = "Will you integrate data from any third party cloud APIs?";
    //Shared Preference
    public static final String sharedPreference = "MyPrefs";
    public static final String []  KEY = {  "Platform",
                                            "Screens",
                                            "UserManagement",
                                            "Data",
                                            "DataSources",
                                            "LocationData",
                                            "ThirdPartyCloudApis",
                                            "UserEngagement",
                                            "Quality",
                                            "Security",
                                            "Analytics",
                                            "Reliability",
                                            "WebPortal",
                                            "BackEndApplications",
                                            "Offline"  };
    public static final String[] OPTIONS_MESSAGES = { ADD_PLATFORM_MESSAGE,ADD_SCREEN_MESSAGE,ADD_ENTERPRISE_MESSAGE,
                                                            ADD_DATASOURCE_MESSAGE,ADD_DATASTORAGE_MESSAGE,ADD_Location_MESSAGE,
                                                            ADD_CLOUD_MESSAGE,ADD_USER_MESSAGE,ADD_QUALITY_MESSAGE,ADD_SECURITY_MESSAGE,
                                                            ADD_ANALYTICS_MESSAGE,ADD_SCALIBILITY_MESSAGE,ADD_PORTAL_MESSAGE,ADD_BACKEND_APPLICATIONS,ADD_OFFLINE_MESSAGE
                                                            };
    public static final String OVERVIEW_KEY = "OverViewList";
    public static final boolean[] isSingleSelect= {false,true,false,false,true,true,true,false,true,true,true,true,true,true,true};
    public static final String[] HEADER_STRINGS={
            "What platform will your app run on?",
            "How big is your app going to be?",
            "How will you handle user management?",
            "what kind of date will you store?",
            "Will you need to integrate any existing data sources?",
            "Will your app use location data?",
            "Will you integrate data from any third party cloud APIs?",
            "How will you engage with your users?",
            "What will be the quality of your experience?",
            "What level of security will you need?",
            "what kind 0f analytics data will you track?",
            "How important reliability/scale be to your app?",
            "Will you need a web portal to administer your app?",
            "Will your app leverage an existing backend application?",
            "Will your app need to work in offline mode?"
    };
    public static final String SINGLE_OPTION = "*You can choose a single option";
    public static final String MULTIPLE_OPTION ="*You can choose multiple options";
    public static final int SLIDER_COLOMNS = 1,GRIDVIEW_COLOMNS =2 ,ZERO_POINTER =8;
    public static final int POSITION_KEY = 1,FRAGMENT_COUNTER_KEY = 2;
}
