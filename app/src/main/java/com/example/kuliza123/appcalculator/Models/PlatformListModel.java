package com.example.kuliza123.appcalculator.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kuliza123 on 29/7/15.
 */
public class PlatformListModel implements Parcelable {
    private int image;
    private int price;
    private String name;
    private int time;
    private Boolean IsSelected;

    public PlatformListModel(int image, int price, String name, Boolean isSelected,int time) {
        this.image = image;
        this.price = price;
        this.name = name;
        this.time = time;
        IsSelected = isSelected;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setTime(int time){this.time = time; }

    public int getTime(){ return this.time;}

    public Boolean getIsSelected() {
        return IsSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        IsSelected = isSelected;
    }


    protected PlatformListModel(Parcel in) {
        image = in.readInt();
        price = in.readInt();
        name = in.readString();
        byte IsSelectedVal = in.readByte();
        IsSelected = IsSelectedVal == 0x02 ? null : IsSelectedVal != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(image);
        dest.writeInt(price);
        dest.writeString(name);
        if (IsSelected == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (IsSelected ? 0x01 : 0x00));
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PlatformListModel> CREATOR = new Parcelable.Creator<PlatformListModel>() {
        @Override
        public PlatformListModel createFromParcel(Parcel in) {
            return new PlatformListModel(in);
        }

        @Override
        public PlatformListModel[] newArray(int size) {
            return new PlatformListModel[size];
        }
    };
}